<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<link rel="stylesheet" type="text/css" href="css/flightindicators.css" />
<script type="text/javascript" src="smoothie.js"></script>  
<script type="text/javascript" src="progressbar.js"></script>
<script src="jquery.flightindicators.js"></script>
<script>
function openDevice(evt, deviceName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(deviceName).style.display = "block";
    evt.currentTarget.className += " active";
	}


/* function log(){
	 $.ajax({
		  url: "LogServlet",
		  type: "GET",
          success: function(){
           console.log("Closing connection..");
        }});
	 console.log("log click connection..");
} */
	

/* $("logout").on('click', function (e){
	e.preventDefault();
	 console.log("log click connection..");
	$.ajax({ 
		url: "MyServlet",
	    type: "GET",
        success: function(){
           console.log("Closing connection..");
        }});
});
 */
 
 function createTimeline() {
     var chart1 = new SmoothieChart({labels:{fillStyle:'#c1a4a4'},timestampFormatter:SmoothieChart.timeFormatter});
     var chart2 = new SmoothieChart({labels:{fillStyle:'#c1a4a4'},timestampFormatter:SmoothieChart.timeFormatter});
    
	chart1.addTimeSeries(Navio1_Altitude_m_line, { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 4 });
	chart1.streamTo(document.getElementById("Navio1_altitude_chart"), 1000);

    chart2.addTimeSeries(Navio1_Airspeed_mps_line,  { strokeStyle: 'rgba(0, 255, 0, 1)', fillStyle: 'rgba(0, 255, 0, 0.2)', lineWidth: 4 });
    chart2.streamTo(document.getElementById("Navio1_airspeed_chart"),  1000);

   };


</script>
<style  media="screen" type="text/css">
#Engine_fuel_box {
  margin: 00px;
  width:  200px;
  height: 00px;
}
#Engine_throttle_box {
  margin: 00px;
  width:  200px;
  height: 00px;
}
#Engine_airfuelmix_box {
  margin: 00px;
  width:  200px;
  height: 00px;
}

div.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}    
    /* Style the buttons inside the tab */
div.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
div.tab button.active {
    background-color: #ccc;
}
    .tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

.rdot {
    height: 15px;
    width: 15px;
    background-color: #FF0000;
    border-radius: 50%;
    display: inline-block;
}
.dot {
    height: 15px;
    width: 15px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
}

</style>

</head>
<body onload="createTimeline()">
<img src="logo.png" width="320" height="88" alt="" />
<h2>Telemetry Website</h2>

<!-- <div id='logout_Box' style="position:absolute; top:120px; left:1800px; z-index:2"> -->
<!-- <input style="position:absolute; top:120px; left:1800px; z-index:2" type="submit" value="Logout"> -->
<a style="position:absolute; top:120px; left:1800px; z-index:2" href="<%=request.getContextPath()%>/Logout">Logout</a>
<!-- </div> -->

<div class="tab">
  <button class="tablinks" onclick="openDevice(event, 'EMS')">Engine</button>
  <button class="tablinks" onclick="openDevice(event, 'Navio1')">Navigation</button>
  <button class="tablinks" onclick="openDevice(event, 'Alarms')">Alarms</button>
  <button class="tablinks" onclick="openDevice(event, 'Inventory')">Aircraft Details</button>
  <button class="tablinks" onclick="openDevice(event, 'Maintenance')">Maintenance</button>
  <button class="tablinks" onclick="openDevice(event, 'History')">History</button>
</div>
 
<div id="EMS" class="tabcontent">
<h3>EMS</h3>
<div id='EMSdataBlock_time_Box' style="position:absolute; top:90px; left:1590px; z-index:2">
	<p>
	      <label for="dataBlock_time"> <center><font size="3">Time</font></center> </label>
	     
	<input type="text" style="text-align:center; font-size:15px" 
	             id = "dataBlock_time"
	                 value = "0"						 					
	                 readonly />
	      </p>
	</div>

 <div id='Engine_rpm' style="position:absolute; top:320px; left:50px; z-index:2">
	     <span id="airspeed"></span>
    </div>
    
    <div id="Engine_fuel_box" style="position:absolute; top:660px; left:30px; z-index:1">   
    <label for="Engine_fuel_box"> <center><font size="5">Fuel</font></center> </label>  
    </div>
    
    <div id="Engine_throttle_box" style="position:absolute; top:660px; left:250px; z-index:1">
    <label for="Engine_throttle_box"> <center><font size="5">Throttle</font></center> </label>     
    </div>
    
    <div id="Engine_airfuelmix_box" style="position:absolute; top:800px; left:140px; z-index:1">
    <label for="Engine_airfuelmix_box"> <center><font size="5">Air----> Fuel</font></center> </label>     
    </div>
   
  	 <div id='Engine_Low_Oil_Box' style="position:absolute; top:320px; left:480px; z-index:2">
        <p>
          <label for="Engine_Low_Oil"> <center><font size="5">Oil Level</font></center> </label>
          <br/>
				<input type="text" style="text-align:center; font-size:20px" 
                   id = "Engine_Low_Oil"
                   value = "NO DATA"                   					 					
                   readonly />
        </p>
	 </div>  
	 
	 <div id='Engine_AuxPump_Box' style="position:absolute; top:440px; left:480px; z-index:2">
        <p>
          <label for="Engine_AuxPump"> <center><font size="5">Aux Pump</font></center> </label>
          <br/>
				<input type="text" style="text-align:center; font-size:20px" 
                   id = "Engine_AuxPump"
                   value = "NO DATA"                   					 					
                   readonly />
        </p>
	 </div>  
	 
	 <div id='Engine_Gps_Box' style="position:absolute; top:600px; left:480px; z-index:2">
        <p>
          <label for="Engine_Gps"> <center><font size="5">GPS Status</font></center> </label>
          <br/>
				<input type="text" style="text-align:center; font-size:20px" 
                   id = "Engine_Gps"
                   value = "NO DATA"                   					 					
                   readonly />
        </p>
	 </div>  
	 
	 <div id='Engine_Started_Box' style="position:absolute; top:720px; left:480px; z-index:2">
        <p>
          <label for="Engine_Started"> <center><font size="5">Engine Status</font></center> </label>
          <br/>
				<input type="text" style="text-align:center; font-size:20px" 
                   id = "Engine_Started"
                   value = "NO DATA"                   					 					
                   readonly />
        </p>
	 </div>  
	   
	<div id='Engine_Inlet_Temp_Box' style="position:absolute; top:320px; left:800px; z-index:2">
	    <p>
	      <label for="Engine_Inlet_Temp"> <center><font size="5">Inlet Temp[C]</font></center> </label>
	      <br/>
	<input type="text" style="text-align:center; font-size:23px" 
	             id = "Engine_Inlet_Temp"
	                 value = "0"						 					
	                 readonly />
	      </p>
	</div>
	
	<div id='Engine_Gearbox_Temp_Box' style="position:absolute; top:440px; left:800px; z-index:2">
	    <p>
	      <label for="Engine_Gearbox_Temp"> <center><font size="5">Gearbox Temp[C]</font></center> </label>
	      <br/>
	<input type="text" style="text-align:center; font-size:23px" 
	             id = "Engine_Gearbox_Temp"
	                 value = "0"						 					
	                 readonly />
	      </p>
	</div>    
	
	<div id='Engine_CylinderHead1_Temp_Box' style="position:absolute; top:600px; left:800px; z-index:2">
	    <p>
	      <label for="Engine_CylinderHead1_Temp"> <center><font size="5">CylinderHead1 Temp[C]</font></center> </label>
	      <br/>
	<input type="text" style="text-align:center; font-size:23px" 
	             id = "Engine_CylinderHead1_Temp"
	                 value = "0"						 					
	                 readonly />
	      </p>
	</div>
	
	 <div id='Engine_CylinderHead2_Temp_Box' style="position:absolute; top:720px; left:800px; z-index:2">
	    <p>
	      <label for="Engine_CylinderHead2_Temp"> <center><font size="5">CylinderHead2 Temp[C]</font></center> </label>
	      <br/>
	<input type="text" style="text-align:center; font-size:23px" 
	             id = "Engine_CylinderHead2_Temp"
	                 value = "0"						 					
	                 readonly />
	      </p>
	</div>
		 	 
	<div id='Engine_Carburetor1_Temp_Box' style="position:absolute; top:320px; left:1145px; z-index:2">
	    <p>
	      <label for="Engine_Carburetor1_Temp"> <center><font size="5">Carburetor1 Temp[C]</font></center> </label>
	      <br/>
	<input type="text" style="text-align:center; font-size:23px" 
	             id = "Engine_Carburetor1_Temp"
	                 value = "0"						 					
	                 readonly />
	      </p>
	</div>
	
	 <div id='Engine_Carburetor2_Temp_Box' style="position:absolute; top:440px; left:1145px; z-index:2">
	    <p>
	      <label for="Engine_Carburetor2_Temp"> <center><font size="5">Carburetor2 Temp[C]</font></center> </label>
	      <br/>
	<input type="text" style="text-align:center; font-size:23px" 
	             id = "Engine_Carburetor2_Temp"
	                 value = "0"						 					
	                 readonly />
	      </p>
	</div>
	
		 
	<div id='Engine_ExhaustGas1_Temp_Box' style="position:absolute; top:600px; left:1145px; z-index:2">
	    <p>
	      <label for="Engine_ExhaustGas1_Temp"> <center><font size="5">Exhaust Gas1 Temp[C]</font></center> </label>
	      <br/>
	<input type="text" style="text-align:center; font-size:23px" 
	             id = "Engine_ExhaustGas1_Temp"
	                 value = "0"						 					
	                 readonly />
	      </p>
	</div>
	
	 <div id='Engine_ExhaustGas2_Temp_Box' style="position:absolute; top:720px; left:1145px; z-index:2">
	    <p>
	      <label for="Engine_ExhaustGas2_Temp"> <center><font size="5">Exhaust Gas2 Temp[C]</font></center> </label>
	      <br/>
	<input type="text" style="text-align:center; font-size:23px" 
	             id = "Engine_ExhaustGas2_Temp"
	                 value = "0"						 					
	                 readonly />
	      </p>
	</div>
	
		 	 
	<div id='Engine_Voltage_Box' style="position:absolute; top:320px; left:1490px; z-index:2">
	    <p>
	      <label for="Engine_Voltage"> <center><font size="5">System Voltage[V]</font></center> </label>
	      <br/>
	<input type="text" style="text-align:center; font-size:23px" 
	             id = "Engine_Voltage"
	                 value = "0"						 					
	                 readonly />
	      </p>
	</div>
			 	 	 
	<div id='Engine_Humidity_Box' style="position:absolute; top:440px; left:1490px; z-index:2">
	    <p>
	      <label for="Engine_Humidity"> <center><font size="5">Humidity</font></center> </label>
	      <br/>
	<input type="text" style="text-align:center; font-size:23px" 
	             id = "Engine_Humidity"
	                 value = "0"						 					
	                 readonly />
	      </p>
	</div>
	
	  	 	 	 
	<div id='Engine_Manifold1_Pressure_Box' style="position:absolute; top:600px; left:1490px; z-index:2">
	    <p>
	      <label for="Engine_Manifold1_Pressure"> <center><font size="5">Manifold1 Pressure</font></center> </label>
	      <br/>
	<input type="text" style="text-align:center; font-size:23px" 
	             id = "Engine_Manifold1_Pressure"
	                 value = "0"						 					
	                 readonly />
	      </p>
	</div>
	
	 	 	 	 
	<div id='Engine_Manifold2_Pressure_Box' style="position:absolute; top:720px; left:1490px; z-index:2">
	    <p>
	      <label for="Engine_Manifold2_Pressure"> <center><font size="5">Manifold2 Pressure</font></center> </label>
	      <br/>
	<input type="text" style="text-align:center; font-size:23px" 
	             id = "Engine_Manifold2_Pressure"
	                 value = "0"						 					
	                 readonly />
	      </p>
	</div>
		
	</div>	
	
	
	<div id="Navio1" class="tabcontent">
	<h3>Navio1</h3>
	 <div id='Navio1_attitude' style="position:absolute; top:400px; left:100px; z-index:2">
	     <span id="attitude"></span>
    </div>  
    <div id='Navio1_heading' style="position:absolute; top:400px; left:550px; z-index:2">
	     <span id="heading"></span>
    </div>  
    <div id='Navio1_div_chart1' style="position:absolute; top:750px; left:80px; z-index:2">
    	  <p>Altitude[m]</p>
        <canvas id="Navio1_altitude_chart" width="400" height="200">
              Your browser does not support HTML5 Canvas.
        </canvas>
    </div>    
    <div id='Navio1_div_chart2' style="position:absolute; top:750px; left:530px; z-index:2">
		  <p>Airspeed[mps]</p>        
        <canvas id="Navio1_airspeed_chart" width="400" height="200">
              Your browser does not support HTML5 Canvas.
        </canvas>
    </div>    
  
    <div id='Navio1_div_box1' style="position:absolute; top:500px; left:1050px; z-index:2">
        <p>
          <label for="Navio1_AHRS_Healthy">AHRS Healthy</label>
          <br/>
				<input type="text" style="text-align:center;" 
                   id = "Navio1_AHRS_Healthy"
                   value = "No Data"
                   readonly />
        </p>
	 </div>
    <div id='Navio1_div_box2' style="position:absolute; top:560px; left:1050px; z-index:2">
        <p>
          <label for="Navio1_IMU_Healthy">IMU Healthy</label>
          <br/>
				<input type="text" style="text-align:center;" 
                   id = "Navio1_IMU_Healthy"
                   value = "No Data"
                   readonly />
        </p>
	 </div>
	 <div id='Navio1_div_box3' style="position:absolute; top:620px; left:1050px; z-index:2">
        <p>
          <label for="Navio1_Airspeed_Healthy">Airspeed Healthy</label>
          <br/>
				<input type="text" style="text-align:center;" 
                   id = "Navio1_Airspeed_Healthy"
                   value = "No Data"
                   readonly />
        </p>
	 </div>
	 <div id='Navio1_div_box4' style="position:absolute; top:820px; left:1050px; z-index:2">
        <p>
          <label for="Navio1_Number_of_Satellites">Number of Satellites</label>
          <br/>
				<input type="text" style="text-align:center;" 
                   id = "Navio1_Number_of_Satellites"
                   value = "No Data"
                   readonly />
        </p>
	 </div>
	 <div id='Navio1_div_box5' style="position:absolute; top:880px; left:1050px; z-index:2">
        <p>
          <label for="Navio1_Flight_Mode">Flight Mode</label>
          <br/>
				<input type="text" style="text-align:center;" 
                   id = "Navio1_Flight_Mode"
                   value = "No Data"
                   readonly />
        </p>
	 </div>
	 <div id='Navio1_div_box6' style="position:absolute; top:940px; left:1050px; z-index:2">
        <p>
          <label for="Navio1_Temperature">Temperature</label>
          <br/>
				<input type="text" style="text-align:center;" 
                   id = "Navio1_Temperature"
                   value = "No Data"
                   readonly />
        </p>
	 </div> 
 	 <div id="Navio1_div_box7" style="position:absolute; top:450px; left:1050px; z-index:2">
 	           <label for="Navio1_div_box7">Throttle</label>
 	 
	 </div>
	</div>
	
	
<div id="Alarms" class="tabcontent">
<h3>Alarms Raised</h3>
<div style="position:absolute; top:300px; left:20px; z-index:2">
<font size="5">
<table>
<tr>
<td>G-force limit approached or exceeded</td>
<td><span class="rdot"></span> &nbsp&nbsp YES</td>
</tr>
<tr></tr>
<tr></tr>
<tr>
<td>Cargo load limit exceeded</td>
<td><span class="rdot"></span> &nbsp&nbsp YES</td>
</tr>
<tr></tr>
<tr></tr>
<tr>
<td>Actuator failure</td>
<td><span class="dot"></span> &nbsp&nbsp NO</td>
</tr>
<tr></tr>
<tr></tr>
<tr>
<td>Engine RPM outside of normal band</td>
<td><span class="dot"></span> &nbsp&nbsp NO</td>
</tr>
<tr></tr>
<tr></tr>
<tr>
<td>Failure of sensor (any)</td>
<td><span class="rdot"></span> &nbsp&nbsp YES</td>
</tr>
<tr></tr>
<tr></tr>
<tr>
<td>Door open or ajar during operation</td>
<td><span class="dot"></span> &nbsp&nbsp NO</td>
</tr>
<tr></tr>
<tr></tr>
<tr>
<td>Tire flat, low on air, or over-pressurized</td>
<td><span class="dot"></span> &nbsp&nbsp NO</td>
</tr>

</table>
</font>
</div>
</div>


	<div id="Inventory" class="tabcontent">
	</div>
	
	<div id="Maintenance" class="tabcontent">
	<h3>Maintenance Requirement</h3>
<div style="position:absolute; top:300px; left:20px; z-index:2">
<font size="5">
<table>
<tr>
<td>Oil change</td>
<td><span class="rdot"></span> &nbsp&nbsp YES</td>
</tr>
<tr></tr>
<tr></tr>
<tr>
<td>Engine overhaul</td>
<td><span class="dot"></span> &nbsp&nbsp NO</td>
</tr>
<tr></tr>
<tr></tr>
<tr>
<td>Gearbox oil change</td>
<td><span class="rdot"></span> &nbsp&nbsp YES</td>
</tr>
<tr></tr>
<tr></tr>
<tr>
<td>Wing fabric replacement </td>
<td><span class="dot"></span> &nbsp&nbsp NO</td>
</tr>

</table>
</font>
</div>
	
	</div>
	<div id="History" class="tabcontent">
	</div>
	
</body>
<script type="text/javascript">

var Navio1_Altitude_m_line = new TimeSeries();
var Navio1_Airspeed_mps_line  = new TimeSeries();

var Engine_Fuel_Bar = new ProgressBar.SemiCircle(Engine_fuel_box, {
	   strokeWidth: 8,
	   easing: 'easeInOut',
		color: '#cc6600',
		trailColor: '#262626',
		trailWidth: 3,
		svgStyle: null,
		/* text: {
			value: '',
			alignToBottom: false
		  },
	    step: (state, Engine_Fuel_Bar) => {
			var value = Math.round(Engine_Fuel_Bar.value() * 100);
			if (value === 0) {
			  Engine_Fuel_Bar.setText('');
			} else {
			  Engine_Fuel_Bar.setText(value);
			}
		  }, */
  duration: 1400,
		from: {color: '#cc6600'},
		to: {color: '#30db28'}

});

var Engine_Throttle_Bar = new ProgressBar.SemiCircle(Engine_throttle_box, {
	   strokeWidth: 8,
	   easing: 'easeInOut',
		color: '#30db28',
		trailColor: '#262626',
		trailWidth: 3,
		//svgStyle: {width: '12%', height: '12%'}
		svgStyle: null,
		/* text: {
			value: '',
			alignToBottom: false
		  },
	    step: (state, Engine_Throttle_Bar) => {
			var value = Math.round(Engine_Throttle_Bar.value() * 100);
			if (value === 0) {
			  Engine_Throttle_Bar.setText('');
			} else {
			  Engine_Throttle_Bar.setText(value);
			}
		  }, */
  duration: 1400,
		from: {color: '#30db28'},
		to: {color: '#cc6600'}
});

var Engine_AirFuelMix_Bar = new ProgressBar.SemiCircle(Engine_airfuelmix_box, {
	   strokeWidth: 8,
	   easing: 'easeInOut',
		color: '#754896',
		trailColor: '#262626',
		trailWidth: 3,
		//svgStyle: {width: '12%', height: '12%'}
		svgStyle: null,
		/* text: {
			value: '',
			alignToBottom: false
		  },
	    step: (state, Engine_AirFuelMix_Bar) => {
			var value = Math.round(Engine_AirFuelMix_Bar.value() * 100);
			if (value === 0) {
			  Engine_AirFuelMix_Bar.setText('');
			} else {
			  Engine_AirFuelMix_Bar.setText(value);
			}
		  }, */
  duration: 1400,
		from: {color: '#754896'},
		to: {color: '#cc6600'}
});

var Navio1_Throttle_Bar = new ProgressBar.Line(Navio1_div_box7, {
	   strokeWidth: 3,
	   easing: 'easeInOut',
		color: '#cc6600',
		trailColor: '#262626',
		trailWidth: 3,
		svgStyle: {width: '30%', height: '150%'}
});

var Engine_rpm    = $.flightIndicator('#Engine_rpm', 'airspeed', {airspeed:0, size:350, showBox : true});

//Flight Indicators Navio
 var Navio1_attitude = $.flightIndicator('#Navio1_attitude', 'attitude', {roll:00, pitch:00, size:350, showBox : true});
 var Navio1_heading  = $.flightIndicator('#Navio1_heading' , 'heading' , {heading:00, size:350, showBox:true});


function fetchdata(){
	 $.ajax({
	  url: 'MyServlet',
	  dataType: 'json',
	  type: 'POST',
	  success: function(response){
		  
		  console.log(response);
		  
		  var time        = response.time;
		  var CHT1        = response.CHT1;
		  var CHT2        = response.CHT2;
		  var EGT1        = response.EGT1;
		  var EGT2        = response.EGT2;
		 var CARB1       = response.CARB1; 
		var CARB2       = response.CARB2;
		 var INLET       = response.INLET;
	     var GEARBOX     = response.GEARBOX;
	     var RPM         = response.RPM;
		  var LOW_OIL     = response.LOW_OIL;
		  var FUEL_LEVEL  = response.FUEL_LEVEL;
		  var AUX_PUMP    = response.AUX_PUMP;
		  var AFR         = response.AFR;
		  var MAN1        = response.manifold_pressure_1;
		  var MAN2        = response.manifold_pressure_2;
		  var START_LED  = response.starter_led;
	     var SYS_VOLTAGE = response.system_voltage;
	     var THROTTLE    = response.throttle;
	     var HUMIDITY    = response.humidity;
	     var GPS_HEALTH  = response.gpsStatus;
	     var AHRS_Healthy = response.AHRS_Healthy;
         var IMU_Healthy = response.IMU_Healthy;
         var Airspeed_Healthy = response.Airspeed_Healthy;
         var GPS_numofsat = response.GPS_numofsat;
         var flight_mode = response.flight_mode;
         var roll_deg = response.roll_deg;
         var pitch_deg = response.pitch_deg;
         var yaw_deg = response.yaw_deg;
         var airspeed_mps = response.airspeed_mps;
         var temp_C = response.temp_C;
         //var file_counter = response.file_counter;
         var throttle_cmd = response.throttle_cmd;
         var altitude_m = response.altitude_m;
	     
	    // console.log(file_counter);
	     
	     //EMS
	     $('#dataBlock_time').val(time);
	     
		  inputVal = document.getElementById("Engine_Low_Oil");
		     if (LOW_OIL == 1) {
		    	inputVal.style.backgroundColor = "red";
		        inputVal.value = "EMPTY";
		     }
		     else {
		    	  inputVal.style.backgroundColor = "green";
		    	  inputVal.value = "FULL"
		     }

		     inputVal = document.getElementById("Engine_AuxPump");
		     if (AUX_PUMP == 0) {
		    	  inputVal.style.backgroundColor = "red";
		        inputVal.value = "OFF";
		     }
		     else {
		    	  inputVal.style.backgroundColor = "green";
		    	  inputVal.value = "ON"
		     }

		     inputVal = document.getElementById("Engine_Gps");
		     if (GPS_HEALTH == 0) {
		    	  inputVal.style.backgroundColor = "red";
		        inputVal.value = "FAIL";
		     }
		     else {
		    	  inputVal.style.backgroundColor = "green";
		    	  inputVal.value = "HEALTHY"
		     }

		     inputVal = document.getElementById("Engine_Started");
		     if (START_LED == 0) {
		    	  inputVal.style.backgroundColor = "red";
		        inputVal.value = "STOP";
		     }
		     else {
		    	  inputVal.style.backgroundColor = "green";
		    	  inputVal.value = "START" 
		     }
		  
		     inputVal = document.getElementById("Engine_Inlet_Temp");
		     inputVal.value = INLET;

		     inputVal = document.getElementById("Engine_Gearbox_Temp");
		     inputVal.value = GEARBOX;

		     inputVal = document.getElementById("Engine_CylinderHead1_Temp");
		     inputVal.value = CHT1;

		     inputVal = document.getElementById("Engine_CylinderHead2_Temp");
		     inputVal.value = CHT2;

		     inputVal = document.getElementById("Engine_Carburetor1_Temp");
		     inputVal.value =  CARB1;

			
		     inputVal = document.getElementById("Engine_Carburetor2_Temp");
		     inputVal.value = CARB2;
			 		
		     inputVal = document.getElementById("Engine_ExhaustGas1_Temp");
		     inputVal.value = EGT1;

		     inputVal = document.getElementById("Engine_ExhaustGas2_Temp");
		     inputVal.value = EGT2;

		     inputVal = document.getElementById("Engine_Voltage");
		     inputVal.value = SYS_VOLTAGE;

		     inputVal = document.getElementById("Engine_Humidity");
		     inputVal.value = HUMIDITY;

		     inputVal = document.getElementById("Engine_Manifold1_Pressure");
		     inputVal.value = MAN1;


		     inputVal = document.getElementById("Engine_Manifold2_Pressure");
		     inputVal.value = MAN2;
		     	     
		     Engine_rpm.setAirSpeed((RPM * 160) / 8000);
		     Engine_Fuel_Bar.set(FUEL_LEVEL);
		     Engine_Throttle_Bar.set(THROTTLE);
		 	 Engine_AirFuelMix_Bar.set(AFR/100);
		 	 
		      
		 	//Navio1 

		 	  Navio1_attitude.setRoll(roll_deg);
		 	  Navio1_attitude.setPitch(pitch_deg);
		 	  Navio1_heading.setHeading(yaw_deg);

		 	  Navio1_Altitude_m_line.append(new Date().getTime(), altitude_m);
		 	  Navio1_Airspeed_mps_line.append(new Date().getTime(), airspeed_mps);


		 	    /* inputVal = document.getElementById("Navio1_File_Counter");
		 	    inputVal.style.backgroundColor = "green";
		 	    inputVal.value = "Number of Files: " + file_counter; */


		 	    inputVal = document.getElementById("Navio1_AHRS_Healthy");
		 	    if (AHRS_Healthy == 1) {
		 	        inputVal.style.backgroundColor = "green";
		 	        inputVal.value = "Healthy";
		 	    }
		 	    else {
		 	    	  inputVal.style.backgroundColor = "red";
		 	    	  inputVal.value = "Failed"
		 	    }

		 	    inputVal = document.getElementById("Navio1_IMU_Healthy");
		 	    if (IMU_Healthy == 1) {
		 	        inputVal.style.backgroundColor = "green";
		 	        inputVal.value = "Healthy";
		 	    }
		 	    else {
		 	    	  inputVal.style.backgroundColor = "red";
		 	    	  inputVal.value = "Failed"
		 	    }

		 	    inputVal = document.getElementById("Navio1_Airspeed_Healthy");
		 	    if (Airspeed_Healthy == 1) {
		 	        inputVal.style.backgroundColor = "green";
		 	        inputVal.value = "Healthy";
		 	    }
		 	    else {
		 	    	  inputVal.style.backgroundColor = "red";
		 	    	  inputVal.value = "Failed"
		 	    }
		 	    inputVal = document.getElementById("Navio1_Number_of_Satellites");
		 	    inputVal.value = GPS_numofsat;

		 	    inputVal = document.getElementById("Navio1_Flight_Mode");
		 	    if (flight_mode == 1) {
		 	    		inputVal.value = "MANUAL";
		 	    }
		 	    else if (flight_mode == 2) {
		 				inputVal.value = "AUTOPILOT";
		 	    }
		 	    else{
		 	    		inputVal.value = "NAVIGATION";
		 	    }


		 	    inputVal = document.getElementById("Navio1_Temperature");
		 	    inputVal.value = temp_C;

		 	    Navio1_Throttle_Bar.set(throttle_cmd/100);      
	  }
	 });
	}
	
	
 $(document).ready(function(){
	 setInterval(fetchdata,500);
	});
 
</script> 
</html>