<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
.gdot {
    height: 15px;
    width: 15px;
    background-color: #00CC00;
    border-radius: 50%;
    display: inline-block;
}
.dot {
    height: 15px;
    width: 15px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
}
.rdot {
    height: 15px;
    width: 15px;
    background-color: #FF0000;
    border-radius: 50%;
    display: inline-block;
}
</style>
</head>
<body>
<img src="logo.png" width="320" height="88" alt="" />
<h2>Telemetry Website</h2>
<a style="position:absolute; top:120px; left:1800px; z-index:2" href="<%=request.getContextPath()%>/Logout"">Logout</a>
<hr>
<center>
<br>
<div  align="center" style="max-width:600px; border:2px solid #ccc;">
<h2>Fleet Status</h2>
<font size="4">
<table>
<tr>
<td>1.</td>
<td><a href="emsData.jsp">OfficeUAV</a></td>
<td><span class="gdot"></span>In Flight</td>
<td><span class="dot"></span>Landed</td>
<td><span class="dot"></span>Alarms</td>
<td><span class="dot"></span>Maintenance Required</td>
</tr>
<tr></tr>
<tr></tr>
<tr></tr>
<tr>
<td>2.</td>
<td><a href="#">Brako</a></td>
<td><span class="dot"></span>In Flight</td>
<td><span class="gdot"></span>Landed</td>
<td><span class="dot"></span>Alarms</td>
<td><span class="dot"></span>Maintenance Required</td>
</tr>
<tr></tr>
<tr></tr>
<tr></tr>
<tr>
<td>3.</td>
<td><a href="#">Kruker</a></td>
<td><span class="dot"></span>In Flight</td>
<td><span class="gdot"></span>Landed</td>
<td><span class="rdot"></span>Alarms</td>
<td><span class="dot"></span>Maintenance Required</td>
</tr>
<tr></tr>
<tr></tr>
<tr></tr>
<tr>
<td>4.</td>
<td><a href="#">Jazz</a></td>
<td><span class="dot"></span>In Flight</td>
<td><span class="gdot"></span>Landed</td>
<td><span class="rdot"></span>Alarms</td>
<td><span class="gdot"></span>Maintenance Required</td>
</tr>
<tr></tr>
<tr></tr>
<tr></tr>
</table>
</font>
<!--  
&nbsp#1&nbsp<a href="emsData.jsp">OfficeUAV</a>&nbsp <span class="gdot"></span> In Flight &nbsp &nbsp<span class="dot"></span> Landed &nbsp &nbsp <span class="dot"></span>Alarms &nbsp &nbsp<span class="gdot"></span>&nbspMaintenance Status
<br><br>
#2&nbsp<a href="#">Brako</a> &nbsp &nbsp <span class="gdot"></span> In Flight &nbsp &nbsp<span class="dot"></span> Landed &nbsp &nbsp <span class="dot"></span>Alarms &nbsp &nbsp<span class="gdot"></span>&nbspMaintenance Status
<br><br>
#3&nbsp<a href="#">Kruker</a> &nbsp &nbsp <span class="dot"></span> In Flight &nbsp &nbsp<span class="gdot"></span> Landed &nbsp &nbsp <span class="dot"></span>Alarms &nbsp &nbsp<span class="dot"></span>&nbspMaintenance Status
<br><br>
&nbsp#4&nbsp<a href="#">Jazz</a>&nbsp &nbsp &nbsp &nbsp&nbsp <span class="dot"></span> In Flight &nbsp &nbsp<span class="gdot"></span> Landed &nbsp &nbsp <span class="gdot"></span>Alarms &nbsp &nbsp<span class="dot"></span>&nbspMaintenance Status
<br><br>-->
</div>
</center>
</body>
</html>