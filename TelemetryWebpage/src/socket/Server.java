package socket;

import java.io.*;
import java.net.*;

import org.json.JSONException;
import org.json.JSONObject;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
 
class Server {
    public static void main(String args[]) throws Exception {
       
        ServerSocket server = new ServerSocket(1234);
        System.out.println("wait for connection on port 1234");
 
        boolean run = true;
        JSONObject json;
        String line = null;
        StringBuilder sb = new StringBuilder();
        while(run) {
            Socket client = server.accept();
            System.out.println("got connection on port 1234");
            BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        
            try {
            	json= new JSONObject(sb.toString());  
    		  } catch (JSONException e) {
    			 e.printStackTrace();
    		}
            
                       
       }
        server.close();
    }
}