package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class ServerServlet
 */
@WebServlet("/ServerServlet")
public class ServerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServerServlet() {
        super();
        
    }

    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
				
			/*HttpSession session = request.getSession();
			
			if(session.getAttribute("server") == null) {
					
				boolean check = available(1234);
				System.out.println("check-->"+check);
								
				try {
					ServerSocket server = new ServerSocket(1234);
					System.out.println("wait for connection on port 1234");
					session.setAttribute("socketserver", server);
					System.out.println("session socketserver attribute set " +server);
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}else {
				boolean check = available(1234);
				System.out.println("check-->"+check);
				
				ServerSocket server = new ServerSocket(1234);
				System.out.println("wait for connection on port 1234");
				session.setAttribute("socketserver ", server);
				System.out.println("session attribute set " +server);
				
				System.out.println("already created and waiting for connection on port 1234");
			}*/
		
		/*System.out.println("doGet..." +server);	
		boolean check = available(1234);
		System.out.println("check-->"+check);
		if(check) {
			 server.close();
			 try {
					server = new ServerSocket(1234);
				} catch (IOException e) {
					e.printStackTrace();
				}
		        System.out.println("close exsisting and wait for connection on port 1234");
		}else {
			try {
				server = new ServerSocket(1234);
			} catch (IOException e) {
				e.printStackTrace();
			}
	        System.out.println("wait for connection on port 1234");
		}*/       
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("doPost Myservlet call from emsData.jsp trying to connect and fetch data");
		
		JSONObject json = null;
        String line = null;
        StringBuilder sb = new StringBuilder();
        HttpSession session = request.getSession(true);
        
        if(session.getAttribute("socketserver") != null) {
        	
        	ServerSocket server = (ServerSocket) session.getAttribute("socketserver");
        	System.out.println("socket is connected to   "+server);
        	
        	
        	Socket client = server.accept(); //The client never accepted the server. 
        	
        	
            System.out.println("got connection on port 1234");
            BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        
            try {
            	json= new JSONObject(sb.toString());  
    		  } catch (JSONException e) {
    			 e.printStackTrace();
    		}
            
            response.setContentType("application/json");
            response.getWriter().write(json.toString());
        }else {
        	System.out.println("socket is not yet initialized");
        }
       /*Socket client = server.accept();
        System.out.println("got connection on port 1234");
        BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
    
        try {
        	json= new JSONObject(sb.toString());  
		  } catch (JSONException e) {
			 e.printStackTrace();
		}
        
        response.setContentType("application/json");
        response.getWriter().write(json.toString());*/
		
	}
	
	private static boolean available(int port) {
		boolean portTaken = false;
	    ServerSocket socket = null;
	    try {
	        socket = new ServerSocket(port);
	    } catch (IOException e) {
	        portTaken = true;
	    } finally {
	        if (socket != null)
	            try {
	                socket.close();
	            } catch (IOException e) {
	            	e.printStackTrace();
	             }
	}
		return portTaken;
	}

}
