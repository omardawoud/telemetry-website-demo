package socket;

import java.io.IOException;
import java.net.ServerSocket;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     * 
     * Creates a LoginServlet object.
     */
    public LoginServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * 
	 * 
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("doPost logservlet request from welcome.jsp validating input and redirecting");
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		ServletContext sc = getServletContext();
		HttpSession session = request.getSession();
		
		// Log-in info must be equal to the following information.
		if(username.equalsIgnoreCase("romaeris") && password.equalsIgnoreCase("romaeris")) {
			
			boolean check = available(1234); //Checks if there are any available bytes from port 1234
			System.out.println("check-->"+check);
			
			if(!check) {
				ServerSocket server = new ServerSocket(1234);
				System.out.println("wait for connection on port 1234");
				session.setAttribute("socketserver", server);
				System.out.println("session socketserver attribute set " +server);
			}
			
			// forwards request from a servlet sc to another resource (response)
			sc.getRequestDispatcher("/fleet.jsp").forward(request, response);
			
		}
		// Outputs Incorrect username or password error on the page.
		else {
			request.setAttribute("message", "Incorrect username or password");
			sc.getRequestDispatcher("/welcome.jsp").forward(request, response);
		}
		
	}
	
	/**
	 * Checks if the selected port is available.
	 * @param port == the current port to be examined
	 * @return
	 */
	private static boolean available(int port) {
		boolean portTaken = false;
	    ServerSocket socket = null;
	    try {
	        socket = new ServerSocket(port);
	    } catch (IOException e) {
	        portTaken = true;
	    } finally {
	        if (socket != null)
	            try {
	                socket.close();
	            } catch (IOException e) {
	            	e.printStackTrace();
	             }
	}
		return portTaken;
	}

}
